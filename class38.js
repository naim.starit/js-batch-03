// closure 

// let num1 = 10; // outer scope
// function hello() {
//     let num1, num2;  // inner scope
//     console.log(num1);
// }

// hello()


// function hello() {
//     let name = "Next Topper";

//     function myName() {
//         console.log("My name is", name);
//     }
//     myName();
// }

// hello()

// function hello() {
//     let name = "Next Topper";

//     function myName() {
//         console.log("My name is", name);
//     }
    
//     return myName;
// }
// var res = hello();
// res();

// function increment() {
//     count = 0;   // like as global variable
//     count += 1;
// }

// increment()
// increment()
// increment()
// increment()
// console.log(count);



// Set  

// "hello next topper"
// let mySet = new Set(); 
// mySet.add(5)
// mySet.add(5)
// mySet.add(6)
// mySet.add(5)
// mySet.add(7)
// mySet.add(4)
// // mySet.delete(5)
// // mySet.clear() 
// var res = mySet.entries()
// console.log(res);

// Md.Kamrul Islam10:46 PM
// var str = "hello next topper";
// let mySet = new Set(str);
// console.log(mySet);

// Humayun Kobir10:46 PM
// let str = 'hello next topper'
// let str1 = str.split('')
// let str2 = new Set(str1)
// console.log(str2.size); 


// var myStack = []; // Priciple: LIFO 

// myStack.push(5)
// myStack.push(6)
// myStack.push(7) 

// myStack.pop()

// console.log(myStack);


// var myQueue = []; // FIFO 

// myQueue.push(5)
// myQueue.push(9)
// myQueue.push(7)
// myQueue.push(3)

// myQueue.shift();

// console.log(myQueue);