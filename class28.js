// function Man(name, age) {
//     this.name = name;
//     this.age = age;
// }

// Man.prototype.speak = function() {
//         console.log("Name:", this.name, "age: ", this.age);
//     }
// Man.prototype.color = "Red";
// Man.prototype = "hello";

// var sakib = new Man("Sakib", 32);
// var tamim = new Man("Tamim", 30);

// sakib.__proto__ = "hi"
// console.log(sakib.__proto__);


// console.log(Man.prototype);
// console.log(tamim.color);

// console.log(sakib.prototype);
// console.log(Man.__proto__);
// sakib.color = "Black";
// console.log(sakib);
// console.log(tamim.color);
// sakib.speak()

// class Animal {
//   // parent
//   constructor(n) {
//     this.name = n;
//   }
//   print() {
//     console.log("Hello Animal");
//   }
// }

// class Man extends Animal {
//   // child
//   _a = 10;
//   constructor(age, name) {
//     super(name);
//     this.age = age;
//   }
//   greet() {
//     console.log("hi Man", this.a);
//   }
//   static run() {
//     console.log("running..............");
//   }
// }

// let sakib = new Man(32, "sakib");
// // Man.
// // Man.run()
// console.log(sakib._a);
// console.log(tamim.name);
// sakib.print();
// sakib.greet();
