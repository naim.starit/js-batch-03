// Execution context 

// let num = 6;
// function multiply(n) {
//     return n * 10
// };
// let result = multiply(num);
// console.log(result);

// 1. assing 6 to the num variable 
// 2. declare a funtion that returns n*10 
// 3. call multiply function with (num=6) and then 
//    assign it to the result variable 
// 4. log the variable result

// each execution context has two phases 
// 1. creation phase 
// 2. execution phase 

//=======================================
//=======================================

// Call Stack 

// function multiply(num1, num2) {
//     return num1 * num2;
// }

// function sum(n1, n2) {
//     return multiply(n1, n2) + 50;
// }

// var res = sum(5, 10);
// console.log(res);