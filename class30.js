// callback hell
// setTimeout(()=>{
//     console.log('This is first log');
//     setTimeout(() => {
//       console.log("This is second log");
//       setTimeout(() => {
//         console.log("This is third log");
//       }, 2000);
//     }, 3000)
// }, 1000)


// Promise 
// A promise is always in one of the three states.
// pending 
// fulfilled 
// rejected 

var isLoggedin = true;
var studentList = [
    "Rahim",
    "Rahi",
    "Rhim",
    "Rhim",
    "Raim",
    "Rhi",
]
// let myPromise = new Promise(function(resolve, reject) {
//     if(isLoggedin) {
//         setTimeout(()=>{
//             resolve(studentList);
//         }, 3000)
//     }
//     else {
//         reject('Please login')
//     }
// });

// myPromise.then(
//     function(result) {console.log(result)},
//     function(error) {console.log(error)}
// )
// myPromise.then(result=>console.log("Student List: ",result))
// .catch(err=>console.log(err));


// https://www.nexttopper.com/all-student 

// Students.then(res=> console.log(res))


// Promise Chainning 

function promiseOne() {
   return new Promise(function (resolve, reject) {
  if (isLoggedin) {
    setTimeout(() => {
      resolve(studentList);
    }, 3000);
  } else {
    reject("Please login");
  }
});
}

let num = 10;
function PromiseTwo(){
    return new Promise(function (resolve, reject) {
  if (num == 10) {
    setTimeout(() => {
      resolve({
          num: 10,
          name: "Promise Two"
      });
    }, 1000);
  } else {
    reject("Please Enter valid number");
  }
});
}

PromiseTwo()
.then(res=> console.log(res))
.then(promiseOne)
.then(result=> console.log(result))
.catch(err=> console.log(err))
