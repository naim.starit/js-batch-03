// Type Conversion 
// 1. implicit type conversion
// 1. explicit type conversion

// implicit type conversion
// var a = "hello" + 20;
// console.log(typeof a);

// explicit type conversion
// var num = "50";
// console.log(typeof Number(num));

//====================================
// to string 
//====================================

// var res = '3' + 5 + 8 + 7;
// console.log(res);  // "3587"

// var res = '5' + true;
// console.log(res);  // "5true"

// var res = '5' + undefined;
// console.log(res);  // "5undefined"

// var res = '5' + null;
// console.log(res);  // "5null"

//====================================
// to number 
//====================================

// var res = '5' - '3';
// console.log(res);  // 2

// var res = '5' - 3;
// console.log(res);  // 2

// var res = '5' * '3';
// console.log(res);  // 15

// var res = '5' ** '3';
// console.log(res);  // 125

// var res = '10' / 2;
// console.log(res);  // 5

// var res = '10' % 2;
// console.log(res);  // 1

//=======================
// to NaN
//========================

// var res = '5' - 'klkdj';
// console.log(res);  // NaN

// var res = 'hell' * 'hello';
// console.log(res);  // NaN

// var res = 5 + 4 * '3' + 10;  // 37(N) // 27(N) // 37(S)
// // 5 + 12 + 10 = 27;
// console.log(res);

// var res = 45 - 5 - 'jh' + 10 + 5; // 24
// console.log(60 + NaN);


//===============================
// boolean to number

// var res = 5 + false;
// console.log(res); // 5

// var res = '5' - true;
// console.log(res); // 4

// var res = true - true;
// console.log(res); // 0

// var res = true + true;
// console.log(res); // 2

//===============================================
// null to number 
// var res = 4 + null;
// console.log(res); // 4
// var res = 4 - null;
// console.log(res); // 4

// var res = 4 + undefined;
// console.log(res); // NaN

// var res = 4 - undefined;
// console.log(res); // NaN

// var res = true - undefined;
// console.log(res); // NaN

// var res = null + undefined;
// console.log(res); // NaN


//==================================================
//==================================================
// Explicit
//==================================================
//==================================================

// to numbner

// console.log(Number('564'));
// console.log(Number(true));
// console.log(Number(false));
// console.log(Number(null));
// console.log(Number(undefined)); //NaN
// console.log(Number('hjd')); //NaN
// console.log(Number(NaN)); //NaN

// var num = 5 + 3 + + '10' + 10;
// console.log(num); //28


// console.log(String(45)); // "45"
// console.log(String(10+10)); // "20"
// console.log(String(null)); // "null"
// console.log(String(undefined)); // "undefined"
// console.log(String(NaN)); // "NaN"
// console.log(String(true)); // "true"
// console.log(String(false)); // "false"

// var num = 12;
// console.log(num.toString());  // params-> base


// Boolean 

// console.log(Boolean(' ')); // true
// console.log(Boolean('')); // false
// console.log(Boolean(null)); // false
// console.log(Boolean(undefined)); // false
// console.log(Boolean(NaN)); // false
// console.log(Boolean(0)); // false
// console.log(Boolean('52454')); // true
// console.log(Boolean('hello')); // true





