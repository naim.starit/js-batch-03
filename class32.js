// var arr = new Array();
// var arr1 = [];

// var regExpression = new RegExp('dsfhjd');
// var re = /dfkj/
// var regExpression = new RegExp("hi", "gi");

// var str = "hi everyone, Hi how hI are you? hi";
// var re = /hi/gi   // global flag, ignore case
// // var pos = str.search(re);
// // console.log(pos);

// var replacedString = str.replace(regExpression, "hello");
// console.log(replacedString);

//===================================================
//flag         ----  gi
//expression       --- [a-z]
//metacharacter   ---   \d
// Quantifier

// var str = "hi everyone, Hi 5 how 5k8 hI are 54 you? 0m8 hi";
// var re = /[045][a-z][0-9]/g;
// var res = str.match(re);
// console.log(res);

// var str = "01f45789652";
// var re = /^[0-9][0-9][a-z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/g;
// var res = re.test(str);
// console.log(res);


//  var str = "hi aabaaa everyone,aaa Hi 5 how a 5k8 hI are 54 you? 0m8 aa hi";
//  var re = /a+/g;   // one or more
//  var res = str.match(re);
//  console.log(res);

//  var str = "hi aabaaa a+ everyone,aaa Hi 5 a* how a 5k8 hI a* are 54 you? 0m8 aa+ hi";
//  var re = /a*/g;   // zero or more
//  var res = str.match(re);
//  console.log(res);

//  var str = "hi aabaaa a+ everyone,aaa Hi 5 a* how a 5k8 hI a* are 54 you? 0m8 aa+ hi";
//  var re = /a\*/g;   // escape quantifier *
//  var res = str.match(re);
//  console.log(res);


// var str = 'apple';
// var re = /^ap/;    // starts with ap
// var res = re.test(str);
// console.log(res);


// var str = 'apple';
// var re = /ple$/;    // ends with ple
// var res = re.test(str);
// console.log(res);


// var str ="hi hkdfjm 48795 kdufo 4548 hkdfiem 7894 kdfi 657 hkdjfm a";
//  var re = /h.*m/g;   // {2, 6}  - {start, end}  {3, } -> {3 to more}
// // var res = str.match(re);
// // console.log(res);

// var res = str.split(" ").filter(ele=> ele.match(re));
// console.log(res);

//  var re = /^[0-9]{4}$/g; // exact 4 digits