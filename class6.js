// var str1, str2, res;
// str1 = 'az';   
// str2 = '1';

// res = (str1 < str2);
// console.log(str2);

// a -> 97  z-> 126
// A -> 65  Z-> 90 

// var num1, num2, num3, res;
// num1 = 8; 
// num2 = 7;
// num3 = 0;

// res = num1 < (num2 === num3); // false
// console.log(res);


// Logical Opearator

//   || -> or  ,   && -> and ,  ! -> not

// true || false   -> true 
// true || true   -> true
// false || true  -> true
// false || false  -> false 

// true && false   ->  false 
// true && true   ->  true    
// false && true  ->  false
// false && false  ->  false

// var num1, num2, num3, res;
// num1 = 8; 
// num2 = 7;
// num3 = 0;

// // res = (num1 !== num3) || (num3 < num1)  // true || true = true
// res = !((num1 == num3) || (num3 < num1));
// // false || true 
// // !true = false
// console.log(res);

// Ternary Opertor    ?  
// var passMarks, myMarks, res, abc;
// passMark = 33;
// myMarks = 12;

// (passMark < myMarks) ? res = "Pass" :
// res = "Fail" ;
// console.log(res);

// console.log(console.log(5));

// function hello() {
//     console.log(5);
// }
// console.log(hello());

// var age, myAge;
// age = 18;
// myAge = 18;

// (age < myAge) ? console.log('You are under 18') : 
// console.log('You are an adult');

var num = 26;
console.log(num.toString(16));

