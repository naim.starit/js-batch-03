// let promise1 = new Promise(function(resolve, reject){
//     setTimeout(()=>{
//         resolve(10)
//     },2000)
// })

// let promise2= new Promise(function(resolve, reject){
//     setTimeout(()=>{
//         resolve(20)
//     },1000)
// })

// var allPromise = [promise1, promise2];

// Promise.all(allPromise)
//   .then((result) => {
//     console.log(result);
//     console.log(result[0]);
//     console.log(result[1]);
//   })
//   .catch((err) => console.log(err));



// async ...... await 

function first() {
   return new Promise(function (resolve, reject) {
  setTimeout(() => {
    resolve("First");
  }, 3000);
})
}

function second() {
   return new Promise(function (resolve, reject) {
  setTimeout(() => {
    resolve("second");
  }, 2000);
})
}

let num = 10;
function third() {
   return new Promise(function (resolve, reject) {
       if(num==9) {
        setTimeout(() => {
        resolve("third");
        }, 1000);
       }
       else {
           reject("this is error")
       }
})
}


// function execute() {
//     second().then((result) => console.log(result));
//     first().then(result=> console.log(result))
//     third().then(result=> console.log(result))
// }

//async .... await

 async function execute() {
  try {
    var res2 = await second();
    console.log(res2);
    var res1 = await first();
    console.log(res1);
    var res3 = await third();
    console.log(res3);
  } catch (e) {
    console.log(e);
  }
}

execute();

// try{

// }
// catch(err) {
//     console.log(err);
// }



// try {
//     let a;
//     console.log(a+ jj);
// }
// catch(err) {
//     console.log("error is", err);
// }

// console.log(2+2);
// console.log("hello");
