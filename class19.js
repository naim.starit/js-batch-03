var student = {
    studentName: "Next",
    age: 12,
    roll: 02,
    className: 'Five',
    parentsInfo: {
        mothersName: "Mrs Y",
        fathersName: "Mr. X"
    }
};
// object destructuring
// var { studentName, age: myAge, roll: myRoll, parentsInfo } = student;

// var { mothersName, fathersName} = parentsInfo;


// console.log(student["age"], student["parentsInfo"]['fathersName']);

// var allKeys = Object.keys(student);
// var allValues= Object.values(student);

// var allEntries = Object.entries(student);

// for(var [k, v] of Object.entries(student)) {
//     console.log('Key:',k,"   ", 'Value:',v);
// }

// for(var k of Object.keys(student)) {
//     console.log("Name:", student['studentName'], '  age: ', student['age']);
// }

// var arr = [
//   {
//     studentName: "Next",
//     age: 12,
//     roll: 02,
//   },
//   {
//     studentName: "Topper",
//     age: 13,
//     roll: 02,
//   },
//   {
//     studentName: "Hello",
//     age: 12,
//     roll: 02,
//   },
// ];

// arr.map(ele=> {
//     console.log(ele.age);
// })

// console.log(allEntries);



var arr = ["studentName", "Next"];

var [k,v] = arr;
console.log(k, v);