// function Student(name, className, age) {
//   this.name = name;
//   this.className = className;
//   this.age = age;
//   this.printName = function () {
//     console.log("My name is", this.name);
//   };
// }


// class Human {
//     constructor(name, age, height) {
//         this.name = name;
//         this.age = age;
//         this.height = height;
//         this.color = "White"
//     }

//    printSomething() {
//         console.log('hello all');
//     }
// }

// class Man {

// }
// var student1 = new Human('Next', 12, '5.7 inch');
// console.log(student1);
// student1.printSomething()


//var let const 

// var num1 = 10;
// num1 = 20;
// var num1 = 40;

// let num2 = 50;
// num2 = 60;
// num2 = 400;

// const num3 = 100;


// global scope  functional scope  block scope 

// var age = 18;
// let animal = "Tiger";
// const maxValue = 9999;

// function hello() {
//     var num1 = 10;
//     let name = "hello";
//     const PI = 3.1416;
//     if(num1 == 10) {
//        var num2;
//        let animal = "Lion"
//        num2 = 100;
//        console.log(num1, num2, PI, name, age);
//        console.log('from Global', age, animal, maxValue);
//     }
//     console.log('another log', num2, animal);
// }

// hello()


// console.log("outside function",age);
// let const -> block scope


// var a;
// a = 100;
// var a;

// let b;
// b=200;
// let b;

// console.log(a, b);


// function sum(num1) {
//     let num2 = 20;
//     console.log(num2);
//     return function add() {
//         var num3;
//         num3 = 100;
//         console.log(num1 + num2 + num3);
//     }
// }

// function hello() {
//     console.log(num2);
// }

// sum(10)()

// hello()


// var res = Number('h')+false+true+5+'6';

// console.log(res);

