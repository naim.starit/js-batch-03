// function factorial(num) {
//   if (num == 0) return 1;
//   else {
//     return num * factorial(num - 1);
//   }
// }
// var res = factorial(20);
// console.log(res);

// factorial(0) -> 1
// factorial(1) -> 1 * 1 = 1
// factorial(2) -> 2 * 1 = 2
// factorial(3) -> 3 * 2 = 6
// console.time("start");
// var res = 1;
// for(var i=20; i>=1; i--) {
//     res*=i;
// }
// console.log(res);
// console.timeEnd("start");

// var num = 45671; // 4+5+6+7+1 = 23
// var remainder, sum=0;
// while(num != 0) {
//     remainder = num % 10;
//     sum += remainder;
//     num = parseInt(num/10);
// }
// console.log(sum);


// call, bind, apply

// implicit binding 

var obj = {
    name: "Next topper",
    age: 45,
    printName: function(age, a, b) {
        console.log(this.name, age, a, b);
    }
}

//explicit binding
var obj1 = {
    name: "Hello"
}

// obj.printName.call(obj1, "5");

// var arr = [56, 6, 7]
// obj.printName.apply(obj1, arr);

var newFunc = obj.printName.bind(obj, 10, 20, 30);
newFunc();



