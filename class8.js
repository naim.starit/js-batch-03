// var age = 50; 
// var res; 

// age < 18 - child 
// 18 - 40 - young 
// 40<age - old

// age<18 ? res="child" : (age>=18 && age<=40) ? res='young' : res='old';
// console.log(res);


// conditional statement 

// if....else 

// var age = 21; 
// var myName = 'Topper';
// var res; 

// 20-30 and next

// if((age>=20 && age<=30) || (myName=='Topper')) {
//    console.log('This is correct');
// }
// else {
//     console.log('Invalid');
// }

//if ... eles if ... else

// if(age == 10) {
//     console.log('your age is', age);
// }
// else if(age == 20) {
//     console.log('your age is', age);
// }
// else if(age == 30) {
//     console.log("your age is", age);
// }
// else {
//     console.log('Invalid age');
// }

// var marks = -8

// A+ A A- B C D F

// if(1<10<3) {
//     // 1<10 -> true  , true<3 , 1<3 = true
//     console.log('hello');
// }
// else {
//     console.log('Hi');
// }

// var marks = 70, result;

// if (marks >= 33 && marks <= 39) {
//   result = "D";
// } else if (marks >= 40 && marks <= 49) {
//   result = "C";
// } else if (marks >= 50 && marks <= 59) {
//   result = "B";
// } else if (marks >= 60 && marks <= 69) {
//   result = "A-";
// } else if (marks >= 70 && marks <= 79) {
//   result = "A";
// } else if (marks >= 80 && marks <= 100) {
//   result = "A+";
// } else {
//   result = "F";
// }

// console.log("You Got :", result, "Grade");