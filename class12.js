// function Expression 
// var myFunc = function () {
//   console.log("Hello I am from parameter");
// };
// myFunc();

// function decleration
// function printSomething() {
//   console.log("hello....");
// }


// arrow function 

// var myArrowFunc = () => {
//   console.log("1st approach")
// };
// myArrowFunc();

//=======================
// var myArrowFunc = () => console.log("2nd approach");
// myArrowFunc();

//=======================
// var myArrowFunc = num => console.log("3rd approach", num);
// myArrowFunc(8);

//=======================
// var myArrowFunc = (num1, num2) => {
//   console.log("4th approach", num1);
//   console.log('Num2 = ', num2);
// }
// myArrowFunc(8, 12);

//============================
// var myArrowFunc = num1 =>  num1 * 5;  
// var res = myArrowFunc(8);  // 40
// console.log(res);

//============================
// var myArrowFunc = num1 => {
//   var num2, num3, sum;
//   num2 = 10, num3 = 20;
//   sum = num1 + num2 + num3;
//   return sum;
// } 
// var res = myArrowFunc(8);  // 40
// console.log(res);

// var myArrowFunc = num => num*2
// var res = myArrowFunc(8);
// console.log(res);


//=============== loop ====================
// for loop 

// console.log("hello");

// for(start ; condition ; increment/decrement) {}

//Given Code
// var num = 5;
// for(var i=1; i<=10 ; i++) {
//   // Print Multiplication table
//   console.log(num,"*",i,"=", num*i);
// }

function multiplicationTable(num) {
  for (var i = 1; i <= 10; i++) {
    // Print Multiplication table
    console.log(num, "*", i, "=", num * i);
  }
}
multiplicationTable(15);
multiplicationTable(5);
multiplicationTable();

// 5 * 1 = 5
// 5 * 2 = 10
// 5 * 3 = 15
// 5 * 4 = 20
// ..........
// ..........


// var num = 1000;
// res = (num*(num+1))/2
// console.log(res);

// var buy, sell, income;
// buy = 500, sell = 700;

// income = sell - buy;

// if(income>0) {
//   console.log('Profit = ', income);
// }
// else {
//   console.log('Loss = ', income * -1);
// }




