// var obj = {
//     name: "Next",
//     // age: 12,
//     height: '5.7',
//     parentsInfo: {
//         mothersName: "Mrs Y",
//         fathersName: "Mr X",
//         address: {
//             division: "Dhaka",
//             address: "Dhanmondi, Dhaka"
//         }
//     }
// }

// if(obj.parentsInfo.hasOwnProperty('address')) {
//     console.log("Your address is: ", obj.parentsInfo.address.address);
// }
// else {
//     console.log("Adress property not available");
// }

// console.log(52+556);

// optional chainning 
// console.log("Your address is: ", obj.parentsInfo?.address?.address);

// if(obj.parentsInfo?.address?.address) {
//     console.log("Your address is: ", obj.parentsInfo.address.address);
// }
// else {
//      console.log("Adress property not available");
// }



// if(obj.hasOwnProperty('age')) console.log('Your age is: ', obj.age);
// else console.log("Age property not available");

// var arr = Object.entries(obj);
// console.log(arr);

// Object.freeze(obj);
// var res = Object.isFrozen(obj);   // return boolean
// obj.name = "black";

// console.log(res);

// var myArr = [
//     ["name", "Next"],
//     ["roll", "12"],
//     ["parentInfo", {mothersName: "Mrs Y",fathersName: "Mr X"}],
// ]

// var obj = Object.fromEntries(myArr);
// console.log(obj.parentInfo.fathersName);

// var arr = [
//     {
//     name: "Next",
//     age: 13,
//     height: '5.7',
//     parentsInfo: {
//         mothersName: "Mrs Y",
//         fathersName: "Mr X",
//         address: {
//             division: "Dhaka",
//             address: "Dhanmondi, Dhaka"
//             }
//         }
//     },
//     {
//     name: "Topper",
//     age: 12,
//     height: '5.7',
//     parentsInfo: {
//         mothersName: "Mrs Y",
//         fathersName: "Mr X",
//         // address: {
//         //     division: "Dhaka",
//         //     address: "Banani, Dhaka"
//         //     }
//         }
//     },
//     {
//     name: "Hello",
//     age: 15,
//     height: '5.7',
//     parentsInfo: {
//         mothersName: "Mrs Y",
//         fathersName: "Mr X",
//         address: {
//             division: "Dhaka",
//             address: "Badda, Dhaka"
//             }
//         }
//     }
// ];

// for (let i = 0; i < arr.length; i++) {
//   console.log(arr[i].name, arr[i].parentsInfo.address.address);
// }

// arr.map(ele => {
//     if (ele.parentsInfo?.address?.address)
//     console.log("Name",ele.name,"Address:",ele.parentsInfo.address.address);
//     else {
//         console.log(ele.name,',Your address is missing');
//     }
// })

// console.log(arr.length);
// console.log(arr);


// var obj = {
//     name: "next",
//     age: 18
// };

// var obj1 = {
//     age: 12,
//     color: "White"
// }

// var obj2 = {
//     roll: 12
// }

// Object.assign(obj, obj1);

// console.log(obj);

// var arr = [
//         ['name', "Next"],
//         ["age", 12],
//         ["isDeleted", false]
//     ];
//     var arrLength = arr.length;
// var obj = {};

// for(var i=0; i<arrLength; i++) {
//     obj[arr[i][0]] = arr[i][1];
//     // console.log(arr[i][0], arr[i][1]);
// }
// console.log(obj);


// var myArr = [
//     ["name", "Next"],
//     ["roll", "12"],
//     ["parentInfo", {mothersName: "Mrs Y",fathersName: "Mr X"}],
// ]
// myArr.map(ele=> {
//     console.log(ele[0], ele[1]*2);
// })
// var obj = Object.fromEntries(myArr);
// console.log(obj.parentInfo.fathersName);

// var arr = [
//     {
//     name: "Next",
//     age: 13,
//     height: '5.7',
//     parentsInfo: {
//         mothersName: "Mrs Y",
//         fathersName: "Mr X",
//         address: {
//             division: "Dhaka",
//             address: "Dhanmondi, Dhaka"
//             }
//         }
//     },
//     {
//     name: "Topper",
//     age: 12,
//     height: '5.7',
//     parentsInfo: {
//         mothersName: "Mrs Y",
//         fathersName: "Mr X",
//         // address: {
//         //     division: "Dhaka",
//         //     address: "Banani, Dhaka"
//         //     }
//         }
//     },
//     {
//     name: "Sakib",
//     age: 15,
//     height: '5.7',
//     parentsInfo: {
//         mothersName: "Mrs Y",
//         fathersName: "Mr X",
//         address: {
//             division: "Dhaka",
//             address: "Badda, Dhaka"
//             }
//         }
//     }
// ];


// arr.forEach(ele=> {
//     // if(ele.age == 12) {
//     //     ele.name = "Hello"
//     // }

//     delete ele.age;
// })

// console.log(arr);

// console.log(
//   student["age"],
//   student["name"],
//   student["parentsInfo"]["mothersName"]
// );