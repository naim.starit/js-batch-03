// JavaScript Hoisting 

// console.log(hello);
// var hello;
// hi()

// function hi() {
//     console.log("hi");
// }

// let num1, num2;

// console.log(2+2);
// const age = 22;


// let num1;
// console.log(num1);


// function declaration
// hi()
// function hi() {
//     console.log("hello");
// }

// function expression 

// let add = function() {
//     console.log(5+5);
// }
// add()

//============================
// const multiply = () => {
//     console.log(10*5);
// }
// multiply()


// if(function () {}) {
//     console.log(typeof f);
// }

// console.log(Boolean(function f(){}));


// var str = "i love my country so much"; 
// var strLength = str.length;
// var uniqueChar = {};

// for(var i=0; i<strLength; i++) {
//     if(str[i] !== ' ') {
//         uniqueChar[str[i]] = ++uniqueChar[str[i]] || 1;
//     }
// }

// var res = Object.entries(uniqueChar).sort((a,b)=> b[1] - a[1])[0];
// console.log(res[0], res[1]);

