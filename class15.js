// var arr = [];
// arr.push(10);
// arr.push(20, 30, 40);

// var arrLength = arr.length;

// var sum = 0;
// for(var i=0; i<arrLength; i++) {
//    sum+=arr[i];
// }
// console.log(sum);

// var arr = [10, 20, 30, 40];
// var arrLength = arr.length;
// for(var i=0; i<arrLength; i++) {
//     arr.pop();
// }
// arr.length = 2;
// console.log(arr);

// var arr = new Array(10);
// arr[5] = 10;
// arr[12] = 100;

// console.log(arr.length);

// rest operator, spread operator

var arr = [10, 20, 30, 40, 50];
// var num1 = 50;
// var num2 = num1;
// var num1 = 100;
// console.log(num1, num2);

var copyArr = [...arr]
arr.push(600, 800, 900);

console.log("Main array", arr);
console.log("Copy Array",copyArr);

// Number, Boolean, String, undefined, null

// arry destructuring
// var [num1, , num3, ...rest] =  arr;
// console.log(num1 + num3);

// var num1 = arr[0];
// var num2 = arr[1];
