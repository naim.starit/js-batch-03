//Comparison Operator 
// ===   !==

// var num1, num2, res, res1, res2;
// num1 = 10;  // number
// num2 = '10'; // string

// res = (num1 == num2);  // == only check values
// res1 = (num1 === num2); // === check values and types
// // console.log(res);
// res2 = (num1 !== num2);
// console.log(res2);

// Assignment operator 

// =, +=, -=, *=, /=, %=
// var num1, num2;
// num1 = 15;
// num2 = 10;

// +=
// num1 = num1 + num2; // 60
// num1 += num2; 

// *=
// num1 = num1 * num2; // 150
// num1 *= num2; 

// /=
//num1 = num1 / num2; // 1.5
//num1 /= num2; 

// %=
//num1 = num1 % num2; // 
//num1 %= num2; 
//console.log(num1);

// string Operator 

var name1, name2, couple;

name1 = "Rabbil";
name2 = "Pori Moni";

couple = name1 + '\t' + name2;   //  \t -> tab  \n -> new line
console.log(couple);

console.log(name1 + ' ' + name2);



