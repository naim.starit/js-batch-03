// map 
// var myMap = new Map(); 

// myMap.set('age', 13); 
// myMap.set(false, 'hello');
// myMap.set(5, 10); 

// myMap.set('a', "apple")
//      .set('b', 'bat')

// console.log(myMap.get('age'));
//  console.log(myMap.get(false)); 

// var keys = myMap.keys();
// console.log(keys.next().value);
// console.log(keys.next().value); 
// console.log(keys.next().value); 

// myMap.delete(5);
//  myMap.clear();
// console.log(myMap); 


// var link = 'https://next-topper.com/get/post/?postNum=5';
// var res = link.split('=')[1];
// console.log(res);


// let data = "data:image/jpeg;base64kldsjffudoisafodufoiui5674usaiodufoiuasdoifuodiufoidf"; 

// var res = data.split(';base64')[0].split('/')[1];
// console.log(res); 


// var arr = ['red', 0, 5, false, 10, true, null, undefined, "black", '', NaN]; 

// // for(var i=0; i<arr.length; i++) {
// //     if(Boolean(arr[i]) == true) {
// //         console.log(arr[i]);
// //     }
// // }

// var res = arr.filter(Boolean)
// console.log(res);  


// &(and) |(or) ^(xor) ~(not)  << (zero fill left shift)  >>(signed right shift)
// >>> (zero fill left shift) 

// 0 & 0 = 0
// 0 & 1 = 0
// 1 & 0 = 0
// 1 & 1 = 1

// 0 | 0 = 0
// 0 | 1 = 1
// 1 | 0 = 1
// 1 | 1 = 1 

// console.log(10&1); 
// 8 4 2 1

// 10&1 

// 1010
// 0001
// -----------------
// 0000 -> 1

// 0101
// 0001
// ---------
// 0101

// console.log(5|1);

// 0101
// 0001  // (^)
// -----------
// 0100
// console.log(5^5); // 4 , 7 , 6, 1 , 0
// console.log(5**25); 


// var arr = [
//     [1,2,3],
//     [4,5,6],
//     [7,8,9]
// ]

// ans = [
//     [1, 4, 7],
//     [8, 5, 2],
//     [3, 6, 9]
// ]